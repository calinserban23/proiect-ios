#import "AddViewController.h"
#import "BoardViewController.h"
#import "AppDelegate.h"

@interface AddViewController ()

@end

@implementation AddViewController

UITextView *textView;
	
-(void)addQuote{
    AppDelegate *appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    NSString *text = [textView text];
    [appDelegate.objects addObject:text];
    [appDelegate.boardTableView reloadData];
    SIOSocket *socket = [AppDelegate getSocket];
    
    [socket emit: @"quotes:add" args:@[[text dataUsingEncoding: NSUTF8StringEncoding]]];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    //first one
    textView = [[UITextView alloc] initWithFrame:CGRectMake(0, 30, 340, 220)];

    // Create a view and add it to the window.
    
    textView.textColor = [UIColor colorWithRed:0/256.0 green:84/256.0 blue:129/256.0 alpha:1.0];
    textView.font = [UIFont fontWithName:@"Helvetica-Bold" size:25];
    textView.backgroundColor=[UIColor grayColor];
    [textView setDelegate:self];
    
    //and so on adjust your view size according to your needs
    //UIView *view = [[UIView alloc] initWithFrame:CGRectMake(20, 30, 600, 400)];
    [self.view addSubview:textView];
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    [button addTarget:self action:@selector(addQuote) forControlEvents:UIControlEventTouchDown];
    [button setTitle:@"Add quote" forState:UIControlStateNormal];
    button.frame = CGRectMake(80.0, 500.0, 160.0, 40.0);
    
    //[view addSubview:button];
    [self.view addSubview:button];
  
//    [view setBackgroundColor: [UIColor yellowColor]];
//    [self.view addSubview: view];
    
    // Create a label and add it to the view.
//    CGRect labelFrame = CGRectMake( 10, 40, 100, 30 );
//    UILabel* label = [[UILabel alloc] initWithFrame: labelFrame];
//    [label setText: @"My Label"];
//    [label setTextColor: [UIColor orangeColor]];
//    [view addSubview: label];
    
    // Create a button and add it to the window.
//    CGRect buttonFrame = CGRectMake( 10, 80, 100, 30 );
//    UIButton *button = [[UIButton alloc] initWithFrame: buttonFrame];
//    [button setTitle: @"My Button" forState: UIControlStateNormal];
//    [button setTitleColor: [UIColor redColor] forState: UIControlStateNormal];
//    [view addSubview: button];
    
    //[self.view makeKeyAndVisible];
    
    //[button release];
    //[label release];
    //[view release];
    
    //return YES;
    
    
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    return YES;
}

- (void)textViewDidChange:(UITextView *)textView
{
    NSLog(@"editing");
    CGFloat fixedWidth = textView.frame.size.width;
    CGSize newSize = [textView sizeThatFits:CGSizeMake(fixedWidth, MAXFLOAT)];
    CGRect newFrame = textView.frame;
    newFrame.size = CGSizeMake(fmaxf(newSize.width, fixedWidth), newSize.height);
    textView.frame = newFrame;
}
	
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [textView endEditing:YES];
}


/*
#pragma mark - Navigation

// In a storyboard-based applicatio		n, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end







































