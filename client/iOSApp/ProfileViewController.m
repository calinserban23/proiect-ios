#import "ProfileViewController.h"
#import "AppDelegate.h"

@interface ProfileViewController () <UITableViewDelegate, UITableViewDataSource>

@property(strong, nonatomic) UITableView *profilesTableView;
@property(strong, nonatomic) NSMutableArray *profilesArray;
@property(strong, nonatomic) NSMutableArray *quotesArray;

@end

@implementation ProfileViewController


-(void)deserializeJson:(NSString*)Json{
    
    NSData* jsonData = [Json dataUsingEncoding:NSUTF8StringEncoding];
    NSError* error = nil;
    id object = [NSJSONSerialization JSONObjectWithData:jsonData options:NSJSONReadingAllowFragments error:&error];
    if([object isKindOfClass:[NSDictionary class]] && error == nil)
    {
        [_quotesArray removeAllObjects];
        _profilesArray = [object objectForKey:@"quotes"];
        for(NSInteger i = 0; i < [_profilesArray count]; ++i)
        {
        NSDictionary *dict = _profilesArray[i];
        NSString *quote = dict[@"quote"];
        [_quotesArray addObject:quote];
        }
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _quotesArray = [[NSMutableArray alloc]init];
    _profilesTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 40, self.view.frame.size.width, self.view.frame.size.height - 49 - 44 - 20 )style:UITableViewStylePlain];
    [_profilesTableView setDelegate:self];
    [_profilesTableView setDataSource:self];
    [_profilesTableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"Cell"];
    
    [self.view addSubview:_profilesTableView];
    SIOSocket *socket = [AppDelegate getSocket];
    [socket emit:(@"quotes:get-list")];
    [socket on: @"quotes:list" callback:^(SIOParameterArray *args){
        [self deserializeJson:args[0]];
        [_profilesTableView reloadData];
    }];
    
    // Do any additional setup after loading the view from its nib.
    [_profilesTableView reloadData];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [_quotesArray count];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString* cellIdentifier = @"Cell";
    UITableViewCell* cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    [[cell textLabel] setNumberOfLines:0];
    [[cell textLabel] setLineBreakMode:UILineBreakModeWordWrap];
    
    if([_quotesArray count] > 0)
    {
    [cell.textLabel setFont:[UIFont fontWithName:@"AvenirNext-Regular" size:15]];
    cell.textLabel.text = _quotesArray[indexPath.row];
    }
    
    return cell;
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString* text = _quotesArray[indexPath.row];
    CGSize textSize = [text sizeWithFont:[UIFont systemFontOfSize: 14.0] forWidth:[tableView frame].size.width - 40.0 lineBreakMode:UILineBreakModeWordWrap];
    return textSize.height < 44.0 ? 44.0 : textSize.height;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
