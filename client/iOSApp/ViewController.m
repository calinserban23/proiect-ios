#import "ViewController.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import "ProfileViewController.h"

@interface ViewController ()


@end
@implementation ViewController

-(void)goToMain{
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([FBSDKAccessToken currentAccessToken])
    {
        UITabBarController *tabBarController = [self.storyboard instantiateViewControllerWithIdentifier:@"tabBarController"];
        tabBarController.selectedIndex = 0;
        [self.navigationController pushViewController:tabBarController animated:YES];
    }
    
    else
    {
    FBSDKLoginButton *loginButton = [[FBSDKLoginButton alloc] init];
    loginButton.center = self.view.center;
    [loginButton addTarget:self action:@selector(goToMain) forControlEvents:UIControlEventTouchDown];
    [self.view addSubview:loginButton];
    }
    //myViewController = [[MyViewController alloc]init];
    // Do any additional setup after loading the view, typically from a nil
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
